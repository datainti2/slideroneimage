import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slideroneimage',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {

  public images: Array<any> = [
    { title: "1", ptitle: "a", n_title: "bioribotr btrbtibjtr bribiernibitur aaa aaaaaavvfvfddccdcdcd", n_image: "https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/covered.jpg" },
    { title: "2", ptitle: "b", n_title: "bb", n_image: "https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/generation.jpg" },
    { title: "3", ptitle: "c", n_title: "cc", n_image: "https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/potter.jpg" },
    { title: "4", ptitle: "d", n_title: "dd", n_image: "https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/preschool.jpg" },
    { title: "5", ptitle: "e", n_title: "ee", n_image: "https://raw.githubusercontent.com/christiannwamba/angular2-carousel-component/master/images/soccer.jpg" }
  ];

  constructor() { }

  ngOnInit() {
  }

}