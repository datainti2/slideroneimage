import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetComponent } from './src/base-widget/base-widget.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetComponent
  ],
  exports: [
    BaseWidgetComponent
  ]
})
export class SliderOneImageModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SliderOneImageModule,
      providers: []
    };
  }
}
